/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * main.c part of https://github.com/rasp/t6963-raspberry
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include "headers.h"






FNT *loadfont(char *name,int from,int to)
{
FNT *font;
int ind;
char buffer[256];

if(!(font = malloc(sizeof(FNT))) )
    {
    perror("loadfont");
    printf("malloc failed\n");
    return NULL;
    }

for(ind = from; ind != to; ind++)
    {
    sprintf(buffer,"%s_%d.bmp",name,ind);
    font->glyp[ind] = (GLY *)load_bmp(buffer);
    }

return font;
}


void char_at(unsigned char c,int x,int y)
{
show_image((IMG *)default_font->glyp[c],x,y);
}

void text_at(char *text,int x, int y)
{
int npos = x;

while(*text)
    {
    char_at(*text,npos,y);
    npos += default_font->glyp[*text]->width;
    text++;
    }

}

void char_at_f(unsigned char c,int x,int y)
{
render((IMG *)default_font->glyp[c],x,y);
}

void text_at_f(char *text,int x, int y)
{
int npos = x;

while(*text)
    {
    char_at_f(*text,npos,y);
    npos += default_font->glyp[*text]->width;
    text++;
    }

}
