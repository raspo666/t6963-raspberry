#include "headers.h"


#define TPATH "/sys/bus/w1/devices/"
#define SENS  "28-81888d116461"   // serial !





void *read_temp(void *wurst)
{
char buffer[256],tval[32];
int sens;

while(1)
    {
    sens = open(TPATH SENS "/w1_slave", O_RDONLY);
    if(sens == -1)
        {
        perror("open sensor failed");
        pthread_exit(0);
        }


    if(read(sens, buffer, 256) > 0)
        {
        strncpy(tval,strstr(buffer,"t=")+2,5);
        pthread_mutex_lock(&m);
        temp = strtof(tval,NULL)/1000;
        pthread_mutex_unlock(&m);
        }
    else
        pthread_exit(0);
    close(sens);
    sleep(1);
    }
}
