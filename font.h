/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * main.c part of https://github.com/rasp/t6963-raspberry
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */



#ifndef _FONT_H
#define _FONT_H





typedef struct {
    unsigned char width;
    unsigned char height;
    unsigned char *data;
}GLY;



typedef struct {
    char *name;
    GLY *glyp[256];
}FNT;


FNT *default_font;

FNT *loadfont(char *name,int from, int to);

void char_at(unsigned char c,int x,int y);
void text_at(char *text,int x, int y);
void char_at_f(unsigned char c,int x,int y);
void text_at_f(char *text,int x, int y);

#endif // _FONT_H
