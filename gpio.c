/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * gpio.c part of https://github.com/rasp/t6963-raspberry
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include "headers.h"


static unsigned short cpins[] = {
	CS,WR,RD,C_D,RST
	};

static unsigned short dpins[] = {
    D0,D1,D2,D3,
    D4,D5,D6,D7
    };



static int  mem_fd;
static void *gpio_map;

// I/O access
volatile unsigned *gpio;



//
// Set up a memory regions to access GPIO
//
void setup_io()
{
   /* open /dev/mem */
   if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
      printf("can't open /dev/mem \n");
      exit(-1);
   }

   /* mmap GPIO */
   gpio_map = mmap(
      NULL,             //Any adddress in our space will do
      BLOCK_SIZE,       //Map length
      PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
      MAP_SHARED,       //Shared with other processes
      mem_fd,           //File to map
      GPIO_BASE         //Offset to GPIO peripheral
   );

   close(mem_fd); //No need to keep mem_fd open after mmap

   if (gpio_map == MAP_FAILED) {
      printf("mmap error %d\n", (int)gpio_map);//errno also set!
      exit(-1);
   }

   // Always use volatile pointer!
   gpio = (volatile unsigned *)gpio_map;


} // setup_io



void d_in() //just set data line to in
{
int i;

for(i = 0; i != 8; i ++)
    {
    INP_GPIO(dpins[i]);
    }
}





unsigned char read_d()  // return state of data lines
{
unsigned char val;
int i;

val = 0;

d_in();

for(i = 0; i != 8; i++)
    {
    //printf("bit %d = %d\n",i,GET_GPIO(dpins[i]));
    if(GET_GPIO(dpins[i]))
        val |= 1<<i;
    }
return val & 0xff;
}


void write_d(unsigned char d)
{
int bit;


for(bit = 0; bit != 8; bit++)
    {
    if(d & (1<<bit))
        {
        INP_GPIO(dpins[bit]);
        OUT_GPIO(dpins[bit]);
        SET_H(dpins[bit]);
        }
    else
        {
        INP_GPIO(dpins[bit]);
        OUT_GPIO(dpins[bit]);
        SET_L(dpins[bit]);
        }
    }


}



void ginit()
{
int i;



  // Set up gpi pointer for direct register access
setup_io();

// set controlpins out && h
for(i = 0; i != 4; i++)
    {
    INP_GPIO(cpins[i]); // must use INP_GPIO before we can use OUT_GPIO ?? *rs
    OUT_GPIO(cpins[i]);
    SET_H(cpins[i]);
    }

// set dataping in
for(i = 0; i != 8; i++)
    {
    INP_GPIO(dpins[i]);
    }

GPIO_PULL = 0;
}


