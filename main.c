/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * main.c part of https://github.com/rasp/t6963-raspberry
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

#include "headers.h"



float temp;
char *pf;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;


void main()
{
int i;
char buffer[256];
IMG *img;
time_t now;
struct tm *local;
char *day[7]={
    "Sun","Mon","Tue","Wed","Thu","Fri","Sat"
    };

pthread_t tid1;


pthread_create(&tid1, NULL, read_temp, NULL);

ginit();

pf = malloc(30*64);





lcd_init();
clr();
set_gr();
clr_gr();


default_font = loadfont("img/uni",32,128);

getchar();
img = load_bmp("logo.bmp");
render(img,20,0);
fastg(pf);

sleep(2);
clr_gr();
while(1)
    {
    if(pthread_kill(tid1,0) != 0)
	    {
        perror("");
        printf("temp sensor thread has left the building\n");
        }
    pthread_mutex_lock(&m);
    sprintf(buffer,"Temp:%.2f C",temp);
    pthread_mutex_unlock(&m);
    text_at_f(buffer,40,3);
    now = time(0);
    local = localtime(&now);
    sprintf(buffer,"%s %02d/%d %02d:%02d:%02d",day[local->tm_wday],local->tm_mday,local->tm_mon+1,local->tm_hour,local->tm_min,local->tm_sec);
    text_at_f(buffer,10,34);
    fastg(pf);
    usleep(900000);
    }

}
