/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * lcd.c part of https://github.com/rasp/t6963-raspberry
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */


#include "headers.h"




void lcd_data(unsigned char data)
{

/* While BUSY1 and BUSY2 are not 1 */
while ( (lcd_read_status() &
    (LCD_STATUS_BUSY1 | LCD_STATUS_BUSY2)) !=
    (LCD_STATUS_BUSY1 | LCD_STATUS_BUSY2));

SET_L(C_D);

write_d(data);

SET_H(RD);
SET_L(WR);
SET_L(CS);

// let it settle
delay();

SET_H(CS);
SET_H(WR);

d_in();

}

unsigned char lcd_read_data(void)
{
unsigned char data;

    /* While BUSY1 and BUSY2 are not 1 */
while ( (lcd_read_status() &
    (LCD_STATUS_BUSY1 | LCD_STATUS_BUSY2)) !=
    (LCD_STATUS_BUSY1 | LCD_STATUS_BUSY2));
SET_L(C_D);
SET_H(WR);
SET_L(RD);
SET_L(CS);
delay();
data = read_d();
SET_H(CS);
SET_H(RD);
return data;

}

void lcd_cmd(unsigned char data)
{

/* While BUSY1 and BUSY2 are not 1 */
while ( (lcd_read_status() &
    (LCD_STATUS_BUSY1 | LCD_STATUS_BUSY2)) !=
    (LCD_STATUS_BUSY1 | LCD_STATUS_BUSY2));

/* Set C/D# */
SET_H(C_D);

write_d(data);

SET_H(RD);
SET_L(WR);
SET_L(CS);
// let it settle
delay();

SET_H(CS);
SET_H(WR);
d_in();

}


unsigned char lcd_read_status()
{
unsigned char d;
SET_H(C_D);
SET_L(RD);
SET_L(CS);
delay();
d = read_d();
SET_H(CS);
SET_H(RD);
return d;
}


void lcd_print_status()
{
char stat;

stat = lcd_read_status();
printf("byte = %x\n",stat);
printf("STA0 = %s\n", stat & 1 ? "ready" : "inexec" );
printf("STA1 = %s\n", stat & 2 ? "ready" : "rw occ");
printf("STA2 = %s\n", stat & 4 ? "ready" : "autoread");
printf("STA3 = %s\n", stat & 8 ? "ready" : "autowrite");
printf("STA4 = %s\n", stat & 0x10 ? "nop" : "nop");
printf("STA5 = %s\n", stat & 0x20 ? "op ok" : "op ?/" );
printf("STA6 = %s\n", stat & 0x40 ? "peek/copy" : "no err" );
printf("STA7 = %s\n", stat & 0x80 ? "blink on" : "blink off");

}



void lcd_init()
{
int i;

SET_L(RST);
usleep(20);
SET_H(RST);

/* From application note */

lcd_cmd(0x80); // OR,CG-ROM

lcd_data(0);
lcd_data(0);
lcd_cmd(0x42); // set graphics home


lcd_data(0x1e);
lcd_data(0x00);
lcd_cmd(0x43); // graphics area set

lcd_data(0x0);
lcd_data(0x0);
lcd_cmd(0x40); // text home

lcd_data(0x1e);
lcd_data(0x00);
lcd_cmd(0x41); // text area set

lcd_data(0x0);
lcd_data(0x0);
lcd_cmd(0x24); // text adddress pointer


lcd_cmd(0x97); //txt/gr on

lcd_cmd(0xa7); // cursor 7 lines

lcd_data(0x0);
lcd_data(0x0);
lcd_cmd(0x21); // set cursor


}



void set_addr(int x, int y)
{

int addr;


addr = (y * 30) + (x / 8) ;
lcd_data(addr & 0xff);
lcd_data((addr >> 8) & 0xff);
lcd_cmd(0x24); //set address pointer

}

void set_gr()
{

lcd_data(0x0);
lcd_data(0x0);
lcd_cmd(0x42); // graphics start

lcd_data(0x1e); // 30 bytes/line
lcd_data(0x00);
lcd_cmd(0x43); // graphics area set

lcd_cmd(0x80); //OR mode

lcd_cmd(0x98); // gr on no txt/curs

set_addr(0,0);
}

void clr()
{
int i;

for (i=0; i< (40*8) ; i++)      //clear screen in text memory
{
lcd_data(0x00);
lcd_cmd(0xC0);
}

lcd_data(0x0);
lcd_data(0x0);
lcd_cmd(0x24); // text adddress pointer

}


void write_ch(char c)
{
lcd_data(c-0x20);  // 'translate' ascii
lcd_cmd(0xc0);
}

void set_cur(int x,int y)
{
lcd_data(x);
lcd_data(y);
lcd_cmd(0x21);
}


void fake_tty(char *txt)
{
char *p;
int r,c,i,x,y;

r = c = i = 0;

p = txt;

while(*p != '\0')
    {
    i++;
    y = i / 40;
    x = i % 40;
    write_ch(*p);
    set_cur(x,y);
    p++;
    usleep(300000);
    }



}

IMG *load_bmp(char *name)
{
FILE *mapf;
BMPHEAD head;
unsigned char *bitmapImage;
BMPINFO info;
IMG *img;

int i;
char *p;



if(!(mapf = fopen(name,"rb")))
    {
    perror("read_bmp");
    printf("while trying to open %s\n",name);
    return NULL;
    }

/*
sizeof(BMPHEAD) gives me 16, not 14,
reading 16 bytes into that struct produces bs....
*/

fread(&head.bfType,1, 2,mapf);
fread(&head.bfSize,1, 4,mapf);
fread(&head.bfReserved1,1, 2,mapf);
fread(&head.bfReserved2,1, 2,mapf);
fread(&head.bOffBits,1, 4,mapf);



if (head.bfType != 0x4D42)
    {
     fclose(mapf);
     printf("%s not a BMP file\n",name);
     return NULL;
    }


if(!(fread(&info, 1,sizeof(BMPINFO),mapf)))
    {
    printf("read info failed on %s\n",name);
    return NULL;
    }


fseek(mapf, head.bOffBits, SEEK_SET);
bitmapImage = (unsigned char*)malloc(info.biSizeImage);
if(bitmapImage == NULL)
    {
    fclose(mapf);
    printf("fail to allocate %d bytes !\n",info.biSizeImage);
    return NULL;
    }

if(fread(bitmapImage,1,info.biSizeImage,mapf) != info.biSizeImage)
    {
    printf("read data %d failed on %s\n",info.biSizeImage,name);
    fclose(mapf);
    return NULL;
    }
fclose(mapf);

if(info.biCompression != 0)
    {
    printf("won't handle compression\n");
    free(bitmapImage);
    return NULL;
    }
if(info.biHeight > 64 || info.biWidth > 240)
    {
    printf("will not display w%d x h%d pixels on this display\n",
            info.biWidth,info.biHeight);
    free(bitmapImage);
    return NULL;
    }
if(info.biBitCount > 1)
    {
    printf("this is a bicolor display !\n");
    free(bitmapImage);
    return NULL;
    }

if(!(img = malloc(sizeof(IMG))))
    {
    printf("no mem for IMG struct ?\n");
    free(bitmapImage);
    return NULL;
    }
img->width = info.biWidth & 0xff;
img->height = info.biHeight & 0xff;
img->data = bitmapImage;
//printf("img data size %d\n",info.biSizeImage);
//printf("img w %d h %d\n",info.biWidth,info.biHeight);

return img;
}



void clr_gr()
{

bzero(pf,1920);

fastg((char *)&pf);

}


unsigned char getpixel(unsigned char x, unsigned char y)
{
unsigned char pixel;

set_addr(x,y);
lcd_cmd(0xc1);
pixel = lcd_read_data() & 1<<(x%8); // WERK is this right ?
return pixel;
}


/* swap bits in a byte from stackover */
unsigned char swap_b(unsigned char b)
{
b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
return b;

}



void setpixel(unsigned char x, unsigned char y,unsigned char pixel)
{
unsigned char b;

set_addr(x,y);

b = 7 - (x%8);
b |= pixel ? 0xf8 : 0xf0;
lcd_cmd(b);
}

void setpixel_m(unsigned char x, unsigned char y,unsigned char pixel)
{
unsigned char b;
char *p;


p = pf + (y * 30) + (x / 8);
b = 7 - (x%8);
if(pixel)
    *p |= 1<<b;
else
    *p &= ~(1<<b);

}




void show_image(IMG *img,int x,int y)
{
int px,py,bc;
char *p,pnum,bi,padBytes;
int wp,left;

p = img->data;
left = 0;

if (((img->width % 32) == 0) || ((img->width % 32) > 24))
    padBytes = 0;
else if ((img->width % 32) <= 8)
    padBytes = 3;
else if ((img->width % 32) <= 16)
    padBytes = 2;
else
    padBytes = 1;


for(py = img->height-1; py > -1; py--)
    {
    set_addr(x,(py+y)); // start line
    for(px = 0; px < img->width   ; px += 8)
        {
        for(bi = 0; bi != 8; bi++)
            {
            setpixel((x + px + bi),(py+y),(*p & 1<<(7 - bi)));
            }
        p++;
        }
    p += padBytes;
    }


}

void render(IMG *img,char x,char y)
{
int px,py,bc;
char *p,pnum,bi,padBytes;
int wp,left;

p = img->data;
left = 0;

if (((img->width % 32) == 0) || ((img->width % 32) > 24))
    padBytes = 0;
else if ((img->width % 32) <= 8)
    padBytes = 3;
else if ((img->width % 32) <= 16)
    padBytes = 2;
else
    padBytes = 1;


for(py = img->height-1; py > -1; py--)
    {
    set_addr(x,(py+y)); // start line
    for(px = 0; px < img->width   ; px += 8)
        {
        for(bi = 0; bi != 8; bi++)
            {
            setpixel_m((x + px + bi),(py+y),(*p & 1<<(7 - bi)));
            }
        p++;
        }
    p += padBytes;
    }


}
















void delay()
{
volatile int i=40;
while(1)
    {
    i--;
    if(i ==0)
        break;
    }
}

void fastwrite(char c)
{

while((lcd_read_status() & 0x8) != 8 );

SET_L(C_D);

write_d(c);

SET_H(RD);
SET_L(WR);
SET_L(CS);

// let it settle
delay();
SET_H(CS);
SET_H(WR);

d_in();
}



void fastg(char *pf)
{
int i;

set_addr(0,0);
lcd_cmd(0xb0); //autowrite on
for(i=0; i!= 1920; i++)
    {
    fastwrite(*pf++);
    }
lcd_cmd(0xb2);




}

