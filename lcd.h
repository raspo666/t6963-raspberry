/*
 * Copyright (C) 2017 Ralph Spitzner
 *
 * lcd.h part of https://github.com/rasp/t6963-raspberry
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 */

/* definitions taken from original example
*  program by Richar taylor
*/
#ifndef _LCD_H
#define _LCD_H

/* Control Word Definitions */
#define LCD_CURSOR_POINTER_SET          0x21
#define LCD_OFFSET_REGISTER_SET         0x22
#define LCD_ADDRESS_POINTER_SET         0x24

#define LCD_TEXT_HOME_ADDRESS_SET       0x40
#define LCD_TEXT_AREA_SET               0x41
#define LCD_GRAPHIC_HOME_ADDRESS_SET    0x42
#define LCD_GRAPHIC_AREA_SET            0x43

#define LCD_CG_ROM_MODE_OR              0x80
#define LCD_CG_ROM_MODE_EXOR            0x81
#define LCD_CG_ROM_MODE_AND             0x83
#define LCD_CG_ROM_MODE_TEXT            0x84
#define LCD_CG_RAM_MODE_OR              0x88
#define LCD_CG_RAM_MODE_EXOR            0x89
#define LCD_CG_RAM_MODE_AND             0x8b
#define LCD_CG_RAM_MODE_TEXT            0x8c

/* 1001 0000 is all off, OR together for ON modes */
#define LCD_DISPLAY_MODES_ALL_OFF       0x90
#define LCD_DISPLAY_MODES_GRAPHICS_ON   0x98
#define LCD_DISPLAY_MODES_TEXT_ON       0x94
#define LCD_DISPLAY_MODES_CURSOR_ON     0x92
#define LCD_DISPLAY_MODES_CURSOR_BLINK  0x91


/* Cursor Pattern Select */
#define LCD_CURSOR_PATTERN_UNDERLINE    0xa0
#define LCD_CURSOR_PATTERN_BLOCK        0xa7

/* Send Auto_XX Command, then block of data, then Auto_reset */
#define LCD_DATA_AUTO_WRITE_SET         0xb0
#define LCD_DATA_AUTO_READ_SET          0xb1
#define LCD_DATA_AUTO_RESET             0xb2

/* Send R/W Then one byte Data */
#define LCD_DATA_WRITE_INCREMENT        0xc0
#define LCD_DATA_READ_INCREMENT         0xc1
#define LCD_DATA_WRITE_NO_INCREMENT     0xc4
#define LCD_DATA_READ_NO_INCREMENT      0xc5

#define LCD_DATA_AUTO_WRITE             0xB0
#define LCD_DATA_AUTO_READ              0xB1
#define LCD_DATA_MODE_RESET             0xB2


#define LCD_SCREEN_PEAK                 0xe0
#define LCD_SCREEN_COPY                 0xe8


#define LCD_SET_BIT                     0xf8
#define LCD_CLR_BIT                     0xf0

/* Status Register Bits */
#define LCD_STATUS_BUSY1    0x01
#define LCD_STATUS_BUSY2    0x02
#define LCD_STATUS_DARRDY   0x08
#define LCD_STATUS_DAWRDY   0x04

#define LCD_STATUS_CLR      0x20
#define LCD_STATUS_ERR      0x40
#define LCD_STATUS_BLINK    0x80
#define FONTWIDTH       6
#define LCD_GRAPHICS_HOME   0x0000  /* This will usually be at the start of RAM */
#define LCD_GRAPHICS_AREA   240/FONTWIDTH    /* A graphic character is 8 bits wide (same as 8x8 char) */
#define LCD_GRAPHICS_SIZE   0x0A00  /* Size of graphics RAM */
#define LCD_TEXT_HOME       0x1200  /* Graphics Area + Text Attribute Size (same size as text size) */
#define LCD_TEXT_AREA       240/FONTWIDTH     /* Text line is x chars wide */
#define LCD_TEXT_SIZE       0x200     /* Size of text RAM */
#define LCD_NUMBER_OF_SCREENS   0x02

#define LCD_COLS     240  // 0 - 29
#define LCD_ROWS     64






/* Function Declarations */
void lcd_init(void);
void lcd_data(unsigned char data);
unsigned char lcd_read_data(void);
void lcd_cmd(unsigned char data);
unsigned char lcd_read_status(void);
void clr();
void write_ch(char);
void fake_tty(char *);
void set_cur(int,int);
void lcd_print_status();

void set_addr(int, int);
void set_gr(); //set graphics mode

/* ugly mode on  */
unsigned char getpixel(unsigned char x, unsigned char y);
void setpixel(unsigned char x, unsigned char y,unsigned char pixel); //pixel 1/0

void setpixel_m(unsigned char x, unsigned char y,unsigned char pixel);


typedef struct{
        unsigned char width;
        unsigned char height;
        unsigned char *data;
}IMG;


/* bitmap (bmp) structs from  stackover */




typedef struct tagBMPHEAD
{
    unsigned short bfType;  //specifies the file type
    unsigned long bfSize;  //specifies the size in bytes of the bitmap file
    unsigned short bfReserved1;  //reserved; must be 0
    unsigned short bfReserved2;  //reserved; must be 0
    unsigned long bOffBits;  //species the offset in bytes from the bitmapfileheader to the bitmap bits
}BMPHEAD;

typedef struct tagBMPINFO
{
    unsigned long biSize;  //specifies the number of bytes required by the struct
    long biWidth;  //specifies width in pixels
    long biHeight;  //species height in pixels
    unsigned short biPlanes; //specifies the number of color planes, must be 1
    unsigned short biBitCount; //specifies the number of bit per pixel
    unsigned long biCompression;//spcifies the type of compression
    unsigned long biSizeImage;  //size of image in bytes
    long biXPelsPerMeter;  //number of pixels per meter in x axis
    long biYPelsPerMeter;  //number of pixels per meter in y axis
    unsigned long biClrUsed;  //number of colors used by th ebitmap
    unsigned long biClrImportant;  //number of colors that are important
}BMPINFO;


void clr_gr();
IMG *load_bmp(char *);
void show_image(IMG *,int,int);
void fastwrite(char);
void sta2();
void fastg(char *);
void render(IMG *,char,char);
void delay();

#endif
