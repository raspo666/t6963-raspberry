CC=gcc
CFLAGS=-I.
LDFLAGS= -lpthread
DEPS = gpio.h headers.h lcd.h font.h Makefile
OBJ = gpio.o lcd.o font.o ds18.o main.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

t6963: $(OBJ)
	gcc -o $@ $^ $(CFLAGS) $(LDFLAGS)


